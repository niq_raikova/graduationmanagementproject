-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2020 at 08:11 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graduation_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `students_import`
--

CREATE TABLE `students_import` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `surname` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `fn` int(5) NOT NULL,
  `specialty` varchar(2) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `degree` enum('bachelor','masters','doctoral','') NOT NULL,
  `grade` decimal(3,2) NOT NULL,
  `username` varchar(250) NOT NULL,
  `gown` varchar(5) NOT NULL,
  `caps` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_import`
--

INSERT INTO `students_import` (`id`, `name`, `surname`, `fn`, `specialty`, `degree`, `grade`, `username`, `gown`, `caps`) VALUES
(118, 'Niya', 'Raykova', 61551, 'SI', 'masters', '6.00', 'niyaray', 'FALSE', 'FALSE'),
(119, 'Ivan', 'Vasilev', 61523, 'KN', 'bachelor', '5.50', 'ivanvas', 'TRUE', 'TRUE'),
(120, 'Maria', 'Dimitrova', 81123, 'IS', 'bachelor', '4.46', 'mariadim', 'FALSE', 'FALSE'),
(121, 'Yana', 'Raykova', 62028, 'IS', 'bachelor', '6.00', 'yanar', 'TRUE', 'TRUE'),
(122, 'Ivan', 'Ivanov', 62323, 'KN', 'masters', '3.30', 'ivaniv', 'FALSE', 'FALSE'),
(123, 'Petko', 'Georgiev', 62340, 'SI', 'bachelor', '5.80', 'petkomg', 'TRUE', 'TRUE');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role` enum('student','admin','administrator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'admin'),
(2, 'georgi', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(3, 'cvetomilag', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(4, 'simonanp', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'student'),
(5, 'administrator', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'administrator'),
(23, 'yanar', '$2y$10$UOJQy8j3VTbZwUpkliqCFuo7GsrXOBIIAv8GL7Ps2z5Goe9yzzLg2', 'student'),
(25, 'petkomg', '$2y$10$uUYcSHRZj9QciA2eYXTpcu2g28HAiI5EXNx7MFYbXd1cyf.oH4HBS', 'student'),
(26, 'ivaniv', '$2y$10$C6S0VhO1qjZiVpn/rHReyOT8IwyDq7TvHxuXP9.0Ddv5RG1o7hvrq', 'student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students_import`
--
ALTER TABLE `students_import`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fn` (`fn`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students_import`
--
ALTER TABLE `students_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=124;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
