-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 19, 2020 at 01:46 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graduation_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `students_import`
--

CREATE TABLE `students_import` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `surname` varchar(256) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `fn` int(5) NOT NULL,
  `specialty` varchar(2) CHARACTER SET cp1251 COLLATE cp1251_bulgarian_ci NOT NULL,
  `degree` enum('bachelor','masters','doctoral','') NOT NULL,
  `grade` decimal(3,2) NOT NULL,
  `username` varchar(250) NOT NULL,
  `gown` varchar(5) NOT NULL,
  `caps` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_import`
--

INSERT INTO `students_import` (`id`, `name`, `surname`, `fn`, `specialty`, `degree`, `grade`, `username`, `gown`, `caps`) VALUES
(1, 'Simona', 'Petrova', 81523, 'KN', 'bachelor', '5.00', 'simonanp', '0', ''),
(2, 'Rosiana', 'Ladzheva', 81461, 'AI', 'bachelor', '6.00', 'rosiana', '0', ''),
(3, 'Tsvetomila', 'Georgieva', 81542, 'KN', 'masters', '6.00', 'cvetomilag', '0', ''),
(4, 'Niya', 'Raykova', 61551, 'SI', 'masters', '6.00', 'niyaray', '0', ''),
(6, 'Maria', 'Dimitrova', 81123, 'IS', 'bachelor', '4.46', 'mariadim', '0', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students_import`
--
ALTER TABLE `students_import`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `fn` (`fn`),
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students_import`
--
ALTER TABLE `students_import`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
