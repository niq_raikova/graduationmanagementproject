-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 12 фев 2020 в 20:51
-- Версия на сървъра: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `graduation_db`
--

-- --------------------------------------------------------

--
-- Структура на таблица `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `role` enum('student','admin','administrator') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Схема на данните от таблица `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin2', '$2y$12$Hf4oD.46Bi9EvXvU1ObkRe5UpngfV0ap8hTvLOj6frQtXqBqlFQoS', 'admin'),
(14, 'admin', '$2y$10$GJT/6vD8VAVR9BFDvnlAneYh.XmLPQ90v.morZ02aaR.2.MlLC0SK', 'student'),
(17, 'g', '$2y$10$kHeR8.F5o.ZnsrA7A549r.itpD5oeLd3TrXaWdyq9ftoiCV1BtKTW', 'administrator'),
(18, 'gg', '$2y$10$IXKfa2Znwto52kRrvMrh3OHdVNJ/YLT.sFf6Vme1OkVFElQfwfHD6', 'student'),
(21, 'cvetomilag', '$2y$10$B00fxKAKpFRVxNLje309v.iOr9Lwv5GN/bnRGfu9KmGJuopH1Orzu', 'student'),
(22, 'simonanp', '$2y$10$vMy.KpD3YpGMYk4awMaLlOIu1S3KLsmxNKofBPNAfP3vKmJ.8oVSW', 'administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
