<?php 

    $router = new Router();
	
    $router->route('POST', 'register', function() {
        UserController::register();
    });

    $router->route('POST', 'login', function() {
        UserController::login();
    });

    $router->route('GET', 'logout', function(){
        UserController::logout();
    });  

	$router->route('GET', 'grantAccess', function(){
        AdminController::loadGrantAccess(); 
    });  
	
	$router->route('POST', 'grantAccess/administrative', function(){
        AdminController::grantAdministrativeAccess(); 
    }); 
	
	$router->route('GET', 'deleteAccount', function(){
        AdminController::loadDeleteAccount(); 
    }); 
	
	$router->route('POST', 'deleteAccount', function(){
        AdminController::deleteAccount(); 
    }); 
	
	$router->route('GET', 'resetAccountPassword', function(){
        AdminController::loadResetAccountPassword(); 
    }); 
	
	$router->route('POST', 'resetAccountPassword', function(){
        AdminController::resetAccountPassword(); 
    });
	
	$router->route('GET', 'listUsers', function(){
        AdminController::loadListUsers(); 
    }); 
	
	$router->route('POST', 'listUsers', function(){
        AdminController::listUsers(); 
    });
	
    $router->route('GET', 'register', function() {
        UserController::loadRegistration();
    });
	
	$router->route('GET', 'studentsImport', function(){
        AdministratorController::loadStudentsImport(); 
    }); 
	
	$router->route('POST', 'studentsImport', function(){
        AdministratorController::studentsImport(); 
    });
	
	$router->route('GET', 'documentsExport', function(){
        AdministratorController::loadDocumentsExport(); 
    }); 
	
	$router->route('POST', 'getGownsData', function(){
        AdministratorController::getGownsData(); 
    });
	
	$router->route('POST', 'getCapsData', function(){
        AdministratorController::getCapsData(); 
    });
	
	$router->route('POST', 'getGraduatesData', function(){
        AdministratorController::getGraduatesData(); 
    });
	
	$router->route('GET', 'statistics', function() {
		StatisticsController::loadStatistics();
	});

    $router->route('GET', '', function(){
        UserController::index(); 
    }); 

    $router->run();
?>