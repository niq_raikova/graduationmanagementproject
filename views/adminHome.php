<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Admin Home Page</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
	<script src="https://kit.fontawesome.com/c476e48a8c.js" crossorigin="anonymous"></script>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/adminHeader.php"; ?>
	
	<main class="container">
		<div class="welcome">
			<h1>Добре дошли в админ страницата!</h1>
		</div>
	</main>
		 
	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>

</html>
