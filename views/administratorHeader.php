<header>
	<div class="menu-toggle"><i class="fas fa-bars" id="toggle"></i></div>
	<div class="header" id="header">
		<div id="logo">
			<a href=<?php echo ROOT."home" ?>>Graduation Portal</a>
		</div>
		<nav>
			<ul>
				<li><a href=<?php echo ROOT."home" ?>>Начало</a></li>
				<li><a href=<?php echo ROOT."studentsImport" ?>>Добавяне на студенти</a></li>
				<li><a href=<?php echo ROOT."documentsExport" ?>>Генериране на списъци</a></li>
				<li><a href=<?php echo ROOT."statisticsAdmin" ?>>Статистики и отчети</a></li>
				<li><a href=<?php echo ROOT."logout" ?>>Изход</a></li>
			</ul>
		</nav>
	</div>
</header>