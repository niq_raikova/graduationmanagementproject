<header>
	<div class="menu-toggle"><i class="fas fa-bars" id="toggle"></i></div>
	<div class="header" id="header">
		<div id="logo">
			<a href=<?php echo ROOT."home" ?>>Портал за дипломирани студенти</a>
		</div>
		<nav>
			<ul>
				<li><a href=<?php echo ROOT."home" ?>>Начало</a></li>
				<li><a href=<?php echo ROOT."statistics" ?>>Статистики и отчети</a></li>
				<li><a href=<?php echo ROOT."logout" ?>>Изход</a></li>
			</ul>
		</nav>
	</div>
</header>