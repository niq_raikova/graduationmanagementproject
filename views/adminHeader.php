<header>
	<div class="menu-toggle"><i class="fas fa-bars" id="toggle"></i></div>
	<div class="header menu-toggle-hide" id="header">
		<div id="logo">
			<a href=<?php echo ROOT."home" ?>>Портал за дипломирани студенти</a>
		</div>
		<nav>
			<ul>
				<li><a href=<?php echo ROOT."home" ?>>Начало</a></li>
				<li><a href=<?php echo ROOT."grantAccess" ?>>Даване на права</a></li>
				<li><a href=<?php echo ROOT."resetAccountPassword" ?>>Смяна на парола</a></li>
				<li><a href=<?php echo ROOT."deleteAccount" ?>>Изтриване на профил</a></li>
				<li><a href=<?php echo ROOT."listUsers" ?>>Списък с потребители</a></li>
				<li><a href=<?php echo ROOT."logout" ?>>Изход</a></li>
			</ul>
		</nav>
	</div>
</header>