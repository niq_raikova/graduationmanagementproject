<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Grant Access</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
	<script src="https://kit.fontawesome.com/c476e48a8c.js" crossorigin="anonymous"></script>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/adminHeader.php"; ?>
	
    <main class="container">
		<form class="auth-form" method="POST" action="<?php echo LOCATION.'grantAccess/administrative'?>"> 
			<h1 class="page-subtitle">Даване на администраторски права на профил с потребителско име:</h1>
			
			<?php include_once VIEWS_DIR.'/errors.php'; ?>
			<?php include_once VIEWS_DIR.'/success.php'; ?>
			
			<input type="text" name="username" placeholder="Потребителско име" class="page-input" required="required"/>
			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="grantAccess">Записване на промените</button>
			</div>
		</form>
	</main>

	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>

</html>