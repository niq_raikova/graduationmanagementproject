<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - List users</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
	<script src="https://kit.fontawesome.com/c476e48a8c.js" crossorigin="anonymous"></script>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/adminHeader.php"; ?>
	
    <main class="container">
		<form class="form-with-table-result" method="POST" action="<?php echo LOCATION.'listUsers'?>"> 
			<h1 class="page-subtitle">Показване на списък с потребители:</h1>
			
			<select name="role" id="admin-search-by-role" class="page-input" required>
				<option value="all" selected>Всички потребители</option>
				<option value="student">Студенти</option>
				<option value="administrator">Администратори</option>
				<option value="admin">Админи</option>
			</select>

			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="listUsers">Търсене</button>
			</div>
		</form>
		<div id="table-result" class="table-result">
			<?php	
			if(isset($success) && sizeof($success)>0){
				echo "<h2 id=\"admin-search-by-title\" class=\"table-title\">";
				$role = $error[0];
				if($role === 'all'){
					echo 'Всички потребители';
				} else if($role === 'student'){
					echo 'Студенти';
				} else if($role === 'administrator'){
					echo 'Администратори';
				} else {
					echo 'Админи';
				}
				echo "</h2>";
				echo "<table>";
					echo "<tr>";
						echo "<th>Потребителско име</th>";
						echo "<th>Роля</th>";
					echo "</tr>";
					foreach($success as $user) {
					   echo "<tr><td>" . $user['username']. "</td><td>" . $user['role'] . "</td></tr>";
					}
					echo "</table>";
			}
			?>
		</div>
	</main>

	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>

</html>