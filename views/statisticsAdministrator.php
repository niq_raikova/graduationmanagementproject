<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Statistics</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
	<script src="https://kit.fontawesome.com/c476e48a8c.js" crossorigin="anonymous"></script>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/administratorHeader.php"; ?>

	<main class="container">
		<div class="auth-form">
			<h1 class="page-subtitle">Статистики и отчети</h1>	
			<div id="statistics-result" class="table-result">
				<table>
					<?php 
					if(isset($success) && sizeof($success)>0){
					 echo "<tr><td>Брой студенти, заявили, че ще присъстват:</td><td>" . $success[0] . "</td></tr>";
					 echo "<tr><td>Брой студенти, които са върнали тогите си:</td><td>" . $success[1] . "</td></tr>";
					 echo "<tr><td>Брой студенти, които са върнали шапките си:</td><td>" . $success[2] . "</td></tr>";
					 echo "<tr><td>Брой студенти, които са наградени:</td><td>" . $success[3] . "</td></tr>";
					}
					?>
				</table>
			</div>
		</div>
	</main>
	
	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>
</html>