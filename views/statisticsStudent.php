<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Statistics</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/studentHeader.php"; ?>

	<main class="container">
		<div class="auth-form">
			<h1 class="page-subtitle">Статистики и отчети</h1>	
			<div id="statistics-result" class="table-result">
				<table>
			<?php 
			if(isset($success) && sizeof($success)>0){
				if ($success[0] == 1) {
					echo "<tr><td> Заявил ли съм присъствие: да </tr></td>";
				} else {
					echo "<tr><td> Заявил ли съм присъствие: не </tr></td>";
				} 
				 
				
				if($success[1] == 1) {
					echo "<tr><td> Получих ли тога: да</tr></td>";
				} else {
					echo "<tr><td> Получих ли тога: не</tr></td>";
				}
				 
				if ($success[2] == 1) {
					echo "<tr><td> Получих ли шапка: да </tr></td>";
				} else {
					echo "<tr><td> Получих ли шапка: не </tr></td>";
				} 
				//echo "<tr><td> Оценка: $success[2] </tr></td>";
				
				
				if ($success[3] == 1) {
					echo "<tr><td> Получих ли награда: да </tr></td>";
				} else {
					echo "<tr><td> Получих ли награда: не </tr></td>";
				} 
			}
			?>
			</table>
			</div>
		</div>
	</main>
</body>
</html>
