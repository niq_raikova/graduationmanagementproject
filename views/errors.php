<?php  if (isset($error) && count($error) > 0) : ?>
  <div class="error">
  	<?php foreach ($error as $errorMsg) : ?>
  	  <p><?php echo $errorMsg ?></p>
  	<?php endforeach ?>
  </div>
<?php  endif ?>