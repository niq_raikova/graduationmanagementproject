<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Generate Documents</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
	<script src="https://kit.fontawesome.com/c476e48a8c.js" crossorigin="anonymous"></script>
</head>

<body class="background-auth sticky-header">
	<?php require_once VIEWS_DIR."/administratorHeader.php"; ?>
	
    <main class="container">
		<form class = "generateDocument" method="POST" action="<?php echo LOCATION.'getGownsData'?>"> 
			<h1 class="page-subtitle">Генериране на списък за тоги</h1>

			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="getGownsData">Генериране</button>
			</div>
		</form>
		<form class = "generateDocument" method="POST" action="<?php echo LOCATION.'getCapsData'?>"> 
			<h1 class="page-subtitle">Генериране на списък за шапки</h1>

			<div class="page-actions"> 
				<button type="submit" class="page-button page-button-active" name="getCapsData">Генериране</button>
			</div>
		</form>
		<form class = "generateDocument" method="POST" action="<?php echo LOCATION.'getGraduatesData'?>"> 
			<h1 class="page-subtitle">Генериране на списък за ред на дипломиране сортиран по:</h1>
			
			<select name="attribute" id="order-by-attribute" class="page-input" required>
				<option value="grade" selected>Оценка</option>
				<option value="specialty">Специалност</option>
				<option value="degree">Образователна степен</option>
			</select>

			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="getGraduatesData">Генериране</button>
			</div>
		</form>
	</main>

	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>

</html>