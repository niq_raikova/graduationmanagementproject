<!DOCTYPE html>
<html lang="bg">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Graduation Portal - Registration</title>

    <link rel="stylesheet" href=<?php echo ROOT."views/css/style.css"?>>
</head>

<body class="background-auth">
    <div class="auth">
		<form class="auth-form" method="POST" action="<?php echo LOCATION.'register'?>" autocomplete="on"> 
            <h1 class="page-title">Портал за дипломирани студенти</h1>
			<h2 class="page-subtitle">Създаване на профил</h2> 
                
			<?php include_once VIEWS_DIR.'/errors.php'; ?>
				
			<input type="text" name="username" placeholder="Потребителско име" class="page-input" required="required"/>
			<input type="password" name="password" placeholder="Парола" class="page-input" required="required"/>
			<input type="password" name="password_confirm" placeholder="Потвърждение на паролата" class="page-input" required="required"/>

				
			<div class="page-actions">
				<button type="submit" class="page-button page-button-active" name="registration">Регистрация</button>
				<a href=<?php echo ROOT ?> class="page-link">Вход</a>
			</div>
		</form>
	</div>

	<script src=<?php echo ROOT."views/scripts/script.js"?>></script>
</body>

</html>