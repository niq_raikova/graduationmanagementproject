<?php

class StatisticsModel extends BaseModel {

	public function getCountOfConfirmed() {		
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
		
	}
	
	public function getCountOfReceivedGowns() {
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE gown = 'TRUE';";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getCountOfReceivedCaps() {
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE caps = 'TRUE';";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getCountOfAwarded() {
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE grade >= 5.5;";
		$query =  $this->connection->query($sql);
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getUserConfirmation() {
		$currentUser = UserController::getCurrentUsername();
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE username LIKE :username;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":username", $currentUser);
		$query->execute();
	    $result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getUserReceivedGowns() {
	    $currentUser = UserController::getCurrentUsername();
		$currentStudent = new StudentModel();
		$currentStudent->username = $currentUser;
		$fn = $currentStudent->getFnFromUsername();
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE gown = 'TRUE' and fn LIKE :fn;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":fn", $fn);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	public function getUserReceivedCaps() {
        $currentUser = UserController::getCurrentUsername();
		$currentStudent = new StudentModel();
		$currentStudent->username = $currentUser;
		$fn = $currentStudent->getFnFromUsername();
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE caps = 'TRUE' and fn LIKE :fn;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":fn", $fn);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
	
	
	public function getUserAwarded() {
		$currentUser = UserController::getCurrentUsername();
		$sql = "SELECT COUNT(*) AS COUNT FROM students_import WHERE username LIKE :username AND grade >= 5.5;";
		$query = $this->connection->prepare($sql);
		$query->bindParam(":username", $currentUser);
		$query->execute();
		$result = $query->fetch(PDO::FETCH_ASSOC);
		return $result;
	}
}
?>