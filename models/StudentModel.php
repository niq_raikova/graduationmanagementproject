<?php 
class StudentModel extends BaseModel {
    private $table_import;

    public $id;
    public $name;
    public $surname;
    public $fn;
    public $specialty;
    public $degree;
    public $grade;
	public $username;
	public $gown;
	public $caps;


    public function __construct(){
      parent::__construct();
      $this->table_import = "students_import";
    }
	
	public function importStudents() {
      $sql = "INSERT INTO $this->table_import (name, surname, fn, specialty, degree, grade, username, gown, caps) VALUES (:name, :surname, :fn, :specialty, :degree, :grade, :username, :gown, :caps);";

      $query = $this->connection->prepare($sql);
	  
      $query->bindParam(":name", $this->name);
      $query->bindParam(":surname", $this->surname);
      $query->bindParam(":fn", $this->fn);
      $query->bindParam(":specialty", $this->specialty);
      $query->bindParam(":degree", $this->degree);
      $query->bindParam(":grade", $this->grade);
      $query->bindParam(":username", $this->username);
	  $query->bindParam(":gown", $this->gown);
	  $query->bindParam(":caps", $this->caps);

      return $query->execute();
    }
	
	
	public function getFnFromUsername() { 
	 $sql = "SELECT fn FROM $this->table_import WHERE username LIKE :username;";
	 $query = $this->connection->prepare($sql);
	 $query->bindParam(":username", $this->username);
	 $query->execute();
     $userData = $query->fetch(PDO::FETCH_ASSOC)['fn'];
     return $userData;
	}
}
?>