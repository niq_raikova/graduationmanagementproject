<?php 
class DocumentsModel extends BaseModel {
	private $graduatesTable;

    public function __construct(){
      parent::__construct();
	  $this->graduatesTable = "students_import";
    }

	public function getGownsData() {		
		$sql = "SELECT name,fn,gown,surname FROM $this->graduatesTable";
		$query = $this->connection->prepare($sql);
		$query->execute();
		$gownsData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $gownsData;
	}
	
	public function getCapsData() {		
		$sql = "SELECT name,fn,caps,surname FROM $this->graduatesTable";
		$query = $this->connection->prepare($sql);
		$query->execute();
		$capsData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $capsData;
	}
	
	public function getGraduatesData($column) {		
		$sql = "";
		if($column=="grade"){
			$sql = "SELECT name,surname,fn,specialty,degree,grade,gown,caps FROM $this->graduatesTable ORDER BY grade DESC";
		}elseif($column=="specialty"){
			$sql = "SELECT name,surname,fn,specialty,degree,grade,gown,caps FROM $this->graduatesTable ORDER BY specialty ASC";
		}else{
			$sql = "SELECT name,surname,fn,specialty,degree,grade,gown,caps FROM $this->graduatesTable ORDER BY degree ASC";
		}
		$query = $this->connection->prepare($sql);
		$query->execute();
		$graduatesData = $query->fetchAll(PDO::FETCH_ASSOC);
		return $graduatesData;
	}	
}
?>