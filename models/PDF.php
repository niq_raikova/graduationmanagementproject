<?php 
require('./pdf/fpdf182/fpdf.php');
class PDF extends FPDF
{
	private $documentType;

	public function setDocumentType($documentType){
        $this->documentType = $documentType;
    }
	
	// Page header
	function Header()
	{
		if($this->documentType=="gown"){
			$this->Image('./views/images/hats-header.png',null,null,210,40);
		}elseif($this->documentType=="caps"){
			$this->Image('./views/images/hats-header.png',null,null,210,40);
		}else{
			$this->Image('./views/images/diplomas-header.png',null,null,200,40);
		}
		$this->Ln(5);
	}

	// Page footer
	function Footer()
	{
		// Position at 1.5 cm from bottom
		$this->SetY(-15);
		// Arial
		$this->SetFont('Arial','',12);
		// Page number
		$this->Cell(0,10,$this->PageNo(),0,0,'C');
	}
}
?>