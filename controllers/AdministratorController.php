<?php

class AdministratorController extends BaseController {
	
	public function loadStudentsImport(){
		if(UserController::isLogged() && UserController::isAdministrator()) {
			BaseController::load('/studentsImport.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function studentsImport(){
		if(UserController::isLogged() && UserController::isAdministrator()) {
			$success = array();
			$errors = array();
			
			if($_FILES['excelDoc']['name']) {
				$arrFileName = explode('.', $_FILES['excelDoc']['name']);
				if ($arrFileName[1] == 'csv') {
					$handle = fopen($_FILES['excelDoc']['tmp_name'], "r");
					$count = 0;
					while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						$count++;
						if ($count == 1) {
							continue; // skip the heading header of sheet
						}
						
						$student = new StudentModel();
						
						$student->name = $data[0];
						$student->surname = $data[1];
						$student->fn = $data[2];
						$student->specialty = $data[3];
						$student->degree = $data[4];
						$student->grade = $data[5];
						$student->username = $data[6];	
						$student->gown = $data[7];	
						$student->caps = $data[8];	
						
						$SheetUpload = $student->importStudents();
					}
					if ($SheetUpload){
						array_push($success, "Студентите са добавени успешно!");
					} else {
						array_push($errors, "Няма добавени студенти!");
					}
				}
				BaseController::load('/studentsImport.php', $errors, $success);
			}
		} else {
            BaseController::load('/login.php');
        } 
	}
	
	public function loadDocumentsExport(){
		if(UserController::isLogged() && UserController::isAdministrator()) {
			BaseController::load('/documentsExport.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function getGownsData(){
		if(isset($_POST) && isset($_POST['getGownsData'])) {
			if(!UserController::isLogged() || !UserController::isAdministrator()) {
				BaseController::load('/login.php');
			} else{
				$i=0;
				$documentsModel = new DocumentsModel();
				$success = $documentsModel->getGownsData();
			
				$pdf = new PDF();
				$pdf->setDocumentType("gown");
			
				$pdf->SetMargins(5,10);
				$pdf->AddPage();
				$pdf->SetFont('Arial','',14);
				foreach($success as $gownData)
				{
					
					if($gownData["gown"] == "TRUE") {
					$i++;
					$pdf->Cell(10, 5,$i,1,0,'C');
				    $pdf->Cell(65, 5,$gownData["name"].' '.$gownData["surname"],1,0,'C');
					$pdf->Cell(30, 5,$gownData["fn"],1,0,'C');
					$pdf->Cell(35, 5,'',1,1);
					}
					
				}  
				$pdf->Output();
			}			
		} else {
			BaseController::load('/login.php', $errors);
        } 
	}
	
	public function getCapsData(){
		if(isset($_POST) && isset($_POST['getCapsData'])) {
			if(!UserController::isLogged() || !UserController::isAdministrator()) {
				BaseController::load('/login.php');
			} else{
				$i=0;
				$documentsModel = new DocumentsModel();
				$success = $documentsModel->getCapsData();
			
				$pdf = new PDF();
				$pdf->setDocumentType("caps");
			
				$pdf->SetMargins(5,10);
				$pdf->AddPage();
				$pdf->SetFont('Arial','',14);
				foreach($success as $capsData)
				{
					if($capsData["caps"] == "TRUE") {
					$pdf->Cell(10, 5,$i,1,0,'C');
				    $pdf->Cell(65, 5,$capsData["name"].' '.$capsData["surname"],1,0,'C');
					$pdf->Cell(30, 5,$capsData["fn"],1,0,'C');
					$pdf->Cell(35, 5,'',1,1);
					}
				}  
				$pdf->Output();
			}			
		} else {
			BaseController::load('/login.php', $errors);
        } 
	}
	
		public function getGraduatesData(){
		if(isset($_POST) && isset($_POST['getGraduatesData'])) {
			if(!UserController::isLogged() || !UserController::isAdministrator()) {
				BaseController::load('/login.php');
			} else{
				$i=0;
				$column = $_POST['attribute'];	
				$documentsModel = new DocumentsModel();
				$success = $documentsModel->getGraduatesData($column);
				$pdf = new PDF();
				$pdf->setDocumentType("graduates");
			
				$pdf->SetMargins(5,10);
				$pdf->AddPage();
				$pdf->SetFont('Arial','',14);
				foreach($success as $graduatesData)
				{
					$i++;
					$pdf->Cell(10, 5,$i,1,0,'C');
					$pdf->Cell(65, 5,$graduatesData["name"].' '.$graduatesData["surname"],1,0);
					$pdf->Cell(20, 5,$graduatesData["fn"],1,0,'C');
					$pdf->Cell(25, 5,$graduatesData["degree"],1,0,'C');
					$pdf->Cell(15, 5,$graduatesData["specialty"],1,0,'C');
					$pdf->Cell(20, 5,$graduatesData["grade"],1,0,'C');
					$pdf->Cell(35, 5,'',1,1);
				}  
				$pdf->Output();
			}			
		} else {
			BaseController::load('/login.php', $errors);
        } 
	}
	
}
?>