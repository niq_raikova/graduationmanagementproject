<?php

class AdminController extends BaseController {

	public function loadGrantAccess(){
		if(UserController::isLogged() && UserController::isAdmin()) {
			BaseController::load('/grantAccess.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function grantAdministrativeAccess(){
		if(isset($_POST) && isset($_POST['grantAccess'])) {
			if(!UserController::isLogged() || !UserController::isAdmin()) {
				BaseController::load('/login.php');
			} 
			
			$errors = array();
			$success = array();
			
			$user = new UserModel();
			$user->username = $_POST['username'];
			
			if(empty($user->username)){
				array_push($errors, "Потребителското име е задължително поле!");
				BaseController::load('/grantAccess.php', $errors);
				return;
			}
			
			$userData = $user->getUser();
			
			if(empty($userData)) {
				array_push($errors, "Профил с даденото потребителско име не съществува!");
				BaseController::load('/grantAccess.php', $errors);
			} else {
				if($userData['role'] === 'admin'){
					array_push($errors, "Неуспешно обновление. Профилът с потребителско име " . $user->username . " има админ права!");
					BaseController::load('/grantAccess.php', $errors);
				} else if($userData['role'] === 'administrator'){
					array_push($errors, "Неуспешно обновление. Профилът с потребителско име " . $user->username . " вече има администраторски права!");
					BaseController::load('/grantAccess.php', $errors, $success);
				} else {
					$user->grantUserAdministrativeAccess();
					array_push($success, "Промените са записани успешно!");
					BaseController::load('/grantAccess.php', $errors, $success);
				}	
			}
		}
	}

	public function loadDeleteAccount(){
		if(UserController::isLogged() && UserController::isAdmin()) {
			BaseController::load('/deleteAccount.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function deleteAccount(){
		if(isset($_POST) && isset($_POST['deleteAccount'])) {
			if(!UserController::isLogged() || !UserController::isAdmin()) {
				BaseController::load('/login.php');
			} 
			
			$errors = array();
			$success = array();
			
			$user = new UserModel();
			$user->username = $_POST['username'];
			
			if(empty($user->username)){
				array_push($errors, "Потребителското име е задължително поле!");
				BaseController::load('/deleteAccount.php', $errors);
				return;
			}
			
			$userData = $user->getUser();
			
			if(empty($userData)) {
				array_push($errors, "Профил с даденото потребителско име не съществува!");
				BaseController::load('/deleteAccount.php', $errors);
			} else {
				if($userData['role'] === 'admin'){
					array_push($errors, "Неуспешно обновление. Профилът с потребителско име " . $user->username . " има админ права!");
					BaseController::load('/deleteAccount.php', $errors);
				} else {
					$user->deleteAccount();
					array_push($success, "Промените са записани успешно!");
					BaseController::load('/deleteAccount.php', $errors, $success);
				}	
			}
		}
	}
	
	public function loadResetAccountPassword(){
		if(UserController::isLogged() && UserController::isAdmin()) {
			BaseController::load('/resetAccountPassword.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function resetAccountPassword(){
		if(isset($_POST) && isset($_POST['resetAccountPassword'])) {
			if(!UserController::isLogged() || !UserController::isAdmin()) {
				BaseController::load('/login.php');
			} 
			
			$errors = array();
			$success = array();
			
			$user = new UserModel();
			$user->username = $_POST['username'];
			$user->password = $_POST['password'];
			
			if(empty($user->username)){
				array_push($errors, "Потребителското име е задължително поле!");
				BaseController::load('/resetAccountPassword.php', $errors);
				return;
			}
			if(empty($user->password)){
				array_push($errors, "Паролата е задължително поле!");
				BaseController::load('/resetAccountPassword.php', $errors);
				return;
			}
			
			$userData = $user->getUser();
			
			if(empty($userData)) {
				array_push($errors, "Профил с даденото потребителско име не съществува!");
				BaseController::load('/resetAccountPassword.php', $errors);
			} else {
				if($userData['role'] === 'admin'){
					array_push($errors, "Неуспешно обновление. Профилът с потребителско име " . $user->username . " има админ права!");
					BaseController::load('/resetAccountPassword.php', $errors);
				} else {
					$user->resetAccountPassword();
					array_push($success, "Промените са записани успешно!");
					BaseController::load('/resetAccountPassword.php', $errors, $success);
				}	
			}
		}
	}
	
	public function loadListUsers(){
		if(UserController::isLogged() && UserController::isAdmin()) {
			BaseController::load('/listUsers.php');
        } else {
            BaseController::load('/login.php');
        }  
	}
	
	public function listUsers(){
		if(isset($_POST) && isset($_POST['listUsers'])) {
			if(!UserController::isLogged() || !UserController::isAdmin()) {
				BaseController::load('/login.php');
			} 
						
			$role = $_POST['role'];		
			$user = new UserModel();
			$success = $user->getAllUsers($role);
			
			$errors = array();
			array_push($errors, $role);
			BaseController::load('/listUsers.php', $errors, $success);
			
		}
	}
	
}
?>