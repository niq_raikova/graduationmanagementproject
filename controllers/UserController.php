<?php
session_start();

class UserController extends BaseController {
	
    // HTTP Method: POST
    public function register() {
        if(isset($_POST) && isset($_POST['registration'])) {
            $user = new UserModel();

            $user->username = $_POST['username'];
			$user->password = $_POST['password'];
			$user->role = 'student';

            $password_confirm = $_POST['password_confirm'];

            $errors = UserController::validateRegistrationData($user, $password_confirm);

            if(count($errors) == 0) {
                $user->addUser();
		
				BaseController::load('/studentHome.php');
                UserController::setSession($user);
            }
            else {
                BaseController::load('/register.php', $errors);
            } 
        }
    }

    // HTTP Method: POST
    public function login() {
        if(isset($_POST) && isset($_POST['login'])) {
            $user = new UserModel();
            $user->username = $_POST['username'];
            $user->password = $_POST['password'];

            $errors = UserController::validateLoginData($user);

            if(count($errors) == 0) {
                $user_data = $user->getUser();
                if(!empty($user_data)) {
                    $encrypted_password = $user_data['password'];

                    if(!empty($encrypted_password)) {
                        if(password_verify($user->password, $encrypted_password)) {
                            UserController::setSession($user);
                            UserController::index();
                        } else {
                            array_push($errors, "Грешна парола!");
                            BaseController::load('/login.php', $errors);
                        }
                    }
                } else {
                    array_push($errors, "Не съществува профил с даденото потребителско име!");
                    BaseController::load('/login.php', $errors);
                }
            } else {
                BaseController::load('/login.php', $errors);
            }
        }
    }
	
	// HTTP Method: GET
    public function index() {
        if(UserController::isLogged()) {
            if(UserController::isAdmin()){
				BaseController::load('/adminHome.php');
			} elseif(UserController::isAdministrator()){
				BaseController::load('/administratorHome.php');
			} else {
				BaseController::load('/studentHome.php');
			}
        }
        else {
            BaseController::load('/login.php');
        }  
    }
	
	// HTTP Method: GET
    public function logout() {
        session_unset();
        session_destroy();

        BaseController::load('/login.php');
    }
	
	public function loadRegistration() {
        if(UserController::isLogged()) {
			if(UserController::isAdmin()){
				BaseController::load('/adminHome.php');
			} elseif(UserController::isAdministrator()){
				BaseController::load('/administratorHome.php');
			} else {
				BaseController::load('/studentHome.php');
			}
        }
        else {
            BaseController::load('/register.php');
        }
    }
	
	private function validateRegistrationData($user, $password_confirm) {
        $errors = array();
        if(empty($user->username)) { array_push($errors, "Потребителското име е задължително поле!"); }
        if(empty($user->password)) { array_push($errors, "Паролата е задължително поле!"); }
        if(!empty($user->getUser())) { array_push($errors, "Профил с даденото потребителско име вече съществува!"); }
        if(!UserController::isValidUsername($user->username)) { array_push($errors, "Невалидно потребителско име! Потребителското име може да съдържа само малки латински букви и трябва да е с дължина от поне 5 символа!"); } 

        if(strlen($user->password) < 6) { array_push($errors, "Паролата трябва да е с дължина от поне 6 символа!"); }
        if($user->password != $password_confirm) { array_push($errors, "Паролите не съвпадат!"); }
	
        return $errors;
    }
	
	private function validateLoginData($user) {
        $errors = array();

        if(empty($user->username)) { array_push($errors, "Потребителското име е задължително поле!"); }
        if(empty($user->password)) { array_push($errors, "Паролата е задължително поле!"); }

        $username_length = is_string($user->username) ? strlen($user->username) : 0;
        if($username_length <= 0 || $username_length > 255 || !UserController::isValidUsername($user->username)) {
            array_push($errors, "Невалидно потребителско име!");
        }

        $pass_length = is_string($user->password) ? strlen($user->password) : 0;
        if($pass_length <= 0 || $pass_length > 2048) {
            array_push($errors, "Невалидна парола!");
        }

        return $errors;
    }
	
	public function isValidUsername($username){
		return preg_match('/^[a-z]{5,}$/', $username);
	}

    private function setSession($user) {
        $userData = $user->getUser();
        $_SESSION['id'] = $userData['id'];
        $_SESSION['username'] = $userData['username'];
        $_SESSION['role'] = $userData['role'];
    }

	public function getCurrentUsername() {
		return $_SESSION['username'];
	}	
		
    public function isLogged() {
        return isset($_SESSION) && isset($_SESSION['id']);
    }
	
	public function isAdmin() {
        return $_SESSION['role'] === "admin";
    }
	
	public function isAdministrator() {
        return $_SESSION['role'] === "administrator";
    }
}
?>
	